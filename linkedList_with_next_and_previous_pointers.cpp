#include <iostream>

class Element {
public:
    int data;
    Element* next;
    Element* prev;

    Element (int d){
        data=d;
        next=NULL;
        prev=NULL;
    }
};

class List {
private:
    Element* head;
public:
    void add (int d);
    void add (int d, int nr);
    void remove (int nr);
    void show();
    List(){
        head = NULL;
    }
};

void List::show() {
    Element* temp = head;
    while (temp){
        std::cout<<temp->data<<std::endl;
        temp=temp->next;
    }
}

void List::remove(int nr) {
    if (nr == 1){
        Element* temp = head;
        Element* sectemp = temp->next;
        head=temp->next;
        sectemp->prev=NULL;
        delete(temp);
    }else{
        Element* before = head;
        for (int i = 2; i < nr; ++i) {
            before=before->next;
        }
        Element* secbefore = before->next;
        Element* thbefore = before->next->next;
        if (thbefore==NULL){
            before->next=NULL;
        }else{
            before->next=thbefore;
            thbefore->prev=before;
        }
        delete(secbefore);
    }

}

void List::add(int d, int nr) {
    Element* newElement = new Element(d);

    if (head==NULL) {
        head=newElement;
    }else if (nr==1) {
        Element* temp = head;
        head=newElement;
        newElement->next=temp;
        newElement->prev=NULL;
        temp->prev=newElement;
    }else {
        Element* before = head;

        for (int i = 2; i < nr; ++i) {
            before = before->next;

        }

        Element* secbefore = before->next;  //secbefore to element nastepny za before
        before->next = newElement;
        newElement->next = secbefore;
        newElement->prev = before;
        if (secbefore!=NULL){
            secbefore->prev = newElement;
        }
    }
}

void List::add(int d) {
    Element* newElement = new Element(d);

    if (head==NULL) {
        head=newElement;
    }else{
        Element* temp = head;
        head=newElement;
        newElement->next=temp;
        temp->prev=newElement;
    }
}

int main() {
    std::cout << "Hello, World!" << std::endl;
    List* list = new List;
    list->add(3);
    list->add(5);
    list->add(4);
    list->add(7);
    list->add(1);

    list->remove(1);
    list->remove(1);
    list->show();
    return 0;
}